<?php

namespace App\Http\Resources\Api;

use App\Traits\PaginationTrait;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TaskCollection extends ResourceCollection
{
    use PaginationTrait ;

    public function toArray(Request $request): array
    {
        return [
            'data' => TaskResource::collection($this->collection),
            'paginate' => $this->paginationModel($this->resource)
        ];
    }
}
