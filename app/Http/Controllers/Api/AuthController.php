<?php
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Resources\Api\UserResource;
use App\Models\User;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;
class AuthController extends Controller
{
    use ResponseTrait ;

    public function login(LoginRequest $request){


        if (! $token = auth()->attempt($request->validated())) {
            return $this->unauthenticatedReturn();
        }
        $userData = UserResource::make(auth()->user())->setToken($token);
        return $this->response('success', "registered", $userData);
    }
    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request) {

        $user = User::create($request->validated());
        $userData = new UserResource($user->refresh());
        return $this->response('success', "registered", $userData);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();
        return $this->successMsg('User successfully signed out');
    }
    public function userProfile(Request $request) {
        $requestToken = ltrim($request->header('authorization'), 'Bearer ');
        return $this->successData(UserResource::make(auth()->user())->setToken($requestToken));
    }

}
