<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Task\StoreRequest;
use App\Http\Requests\Api\Task\UpdateRequest;
use App\Http\Resources\Api\TaskCollection;
use App\Http\Resources\Api\TaskResource;
use App\Models\Task;
use App\Traits\ResponseTrait;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    use ResponseTrait ;
    public function index()
    {
        $tasks = Task::paginate(5);
        return $this->successData(TaskCollection::make($tasks));
    }

    public function store(StoreRequest $request)
    {

        $task = Task::create($request->validated());

        return $this->successData(TaskResource::make($task));
    }

    public function show($id)
    {
        $task = Task::findOrFail($id);

        return $this->successData(TaskResource::make($task));
    }

    public function update(UpdateRequest $request, $id)
    {
        $task = Task::findOrFail($id);
        $task->update($request->validated());
        return $this->successData(TaskResource::make($task));
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);
        $task->delete();
        return response()->json(['message' => 'Task deleted successfully']);
    }
}
