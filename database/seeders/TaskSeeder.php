<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Task;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        for ($i = 0 ; $i<10 ; $i++) {
            Task::create([
                'title' => 'Sample Task'.$i,
                'description' => 'This is another sample task description.',
            ]);
        }

    }
}
