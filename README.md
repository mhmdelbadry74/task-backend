

## Task Senior Backend Developer 

Building  REST API Functionality 
- User Registration.
-  Authentication.
-  Resource Management.

## Laravel

- Laravel version 10 
- php 8.1
- collection postman : 


## Run Project 

- clone project 
- postman https://documenter.getpostman.com/view/8161346/2sA2xpRobx
- after clone project run 
-  composer install 
- php artisan migrate --seed

